﻿using Huaman_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Huaman_T3.Repositorio
{
    public interface IusuarioRepositorio
    {
        Usuario getUsuario(string nombre,string contraseña);
    }
}
