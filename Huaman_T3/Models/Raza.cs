﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Huaman_T3.Models
{
    public class Raza
    {
        public int id { get; set; }
        public string nombre { get; set; }

        public List<Historia> historia { get; set; }

    }
}
