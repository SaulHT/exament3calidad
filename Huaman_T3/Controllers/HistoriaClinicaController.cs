﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Huaman_T3.DB;
using Huaman_T3.Models;
using Huaman_T3.Repositorio;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Huaman_T3.Controllers
{
    public class HistoriaClinicaController : Controller
    {
        private readonly AppExameContex db;
        private IHistoriaRepositorio IHistoria;
        public HistoriaClinicaController(AppExameContex db, IHistoriaRepositorio IHistoria)
        {
            this.db = db;
            this.IHistoria = IHistoria;
        }
        public IActionResult Index()
        {
            //var datos = db.historias.Include(o => o.raza).Include(o => o.Especie).ToList();
            var datos = IHistoria.lista();

            return View(datos);
        }

        [HttpGet]
        public IActionResult Crear()
        {
            //ViewBag.espe = db.especies.ToList();
            ViewBag.especie = IHistoria.listaEspecie();
            //ViewBag.raza = db.razas.ToList();
            ViewBag.razad = IHistoria.listaRaza();

            return View(new Historia());
        }
        [HttpPost]
        public IActionResult Crear(Historia historia)
        {
           
            if (/*db.historias.Any(o=>o.codigo==historia.codigo)*/
                IHistoria.siexist(historia)|| historia.codigo == null)
            {
                ModelState.AddModelError("codigo", "codigo ya existe");
            }
            if (historia.fecha==null)
            {
                ModelState.AddModelError("fecha", "campo obligatorio");
            }
            if (historia.nombre==null ||historia.nombre=="")
            {
                ModelState.AddModelError("nombre", "campo obligatorio");
            }
            if (historia.fechaNacimiento==null)
            {
                ModelState.AddModelError("fechaNacimineto", "campo obligatorio");
            }
            
            if (historia.nombreDueño == null || historia.nombreDueño == "")
            {
                ModelState.AddModelError("nombreDueño", "campo obligatorio");
            }
            if (historia.direccionDueño==null || historia.direccionDueño=="")
            {
                ModelState.AddModelError("direccionDueño", "campo obligatorio");
            }
            if (historia.telefono==null || historia.telefono=="")
            {
                ModelState.AddModelError("telefono", "campo obligatorio");
            }
            if (historia.email !=null)
            {
                if (!Regex.IsMatch(historia.email, @"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$"))
                    ModelState.AddModelError("email","el formato debe ser correcto"); 
            }


            if (ModelState.IsValid)
            {
                //historia.fecha = DateTime.Now;
                //db.historias.Add(historia);
                //db.SaveChanges();
                IHistoria.crearNuevoHistoria(historia);
                return RedirectToAction("Index");
            }
            ViewBag.especie = IHistoria.listaEspecie();
            ViewBag.razad = IHistoria.listaRaza();
            return View(historia);
        }
    }
}
