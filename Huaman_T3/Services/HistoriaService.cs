﻿using Huaman_T3.DB;
using Huaman_T3.Models;
using Huaman_T3.Repositorio;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Huaman_T3.Services
{
    public class HistoriaService : IHistoriaRepositorio
    {
        private AppExameContex db;
        public HistoriaService(AppExameContex db)
        {
            this.db = db;
        }

        public void crearNuevoHistoria(Historia historia)
        {
            historia.fecha = DateTime.Now;
            db.historias.Add(historia);
            db.SaveChanges();
        }

        public List<Historia> lista()
        {
            return db.historias.Include(o => o.Raza).Include(o => o.Especie).ToList();
        }

        public List<Especie> listaEspecie()
        {
            return db.especies.ToList();
        }

        public List<Raza> listaRaza()
        {
            return db.razas.ToList();
        }

        public bool siexist(Historia historia)
        {
            return db.historias.Any(o => o.codigo == historia.codigo);
        }
    }
}
