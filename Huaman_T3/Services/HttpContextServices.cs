﻿using Huaman_T3.Repositorio;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Huaman_T3.Services
{
    public class HttpContextServices : IhttpContexRepositorio
    {
        private HttpContext context;

        public void setHttpContext(HttpContext context)
        {
            this.context = context;
        }

        public void login(ClaimsPrincipal claims)
        {
            context.SignInAsync(claims);
        }
    }
}
